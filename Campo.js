class Campo{

    //Atributos
    campo=Array();
    _tamaño;

    constructor(){
        this._tamaño=9;
    }

    crearCampo(){

        for(let i=0; i<this._tamaño;i++){

            for(let k=0; k<this._tamaño;k++){

                if(!this.campo[i]){
                    this.campo[i]=Array();
                }
                this.campo[i][k]=new Casilla();
            }
        }
    }

    set tamaño(valor){

        if(typeof valor=== "number"){
            this._tamaño=valor;
        }
    }  
}

