class FrontendBuscaminas {

    miCampo;

    constructor(unCampo) {
        if (unCampo === "") {
            throw new Error("No se asignó un campo para sembrar.");
        } else if (!(unCampo instanceof Campo)) {
            throw new Error("La clase del campo no es la esperada.");
        }

        this.miCampo=unCampo;
    }


    /**
     * Mostrar el Campo
     */

    mostrarCampo() {

        let divBuscaminas = document.getElementById("divBuscaminas");
        for (let fila = 0; fila < this.miCampo._tamaño; fila++) {
            for (let columna = 0; columna < this.miCampo._tamaño; columna++) {

                let casilla = this.miCampo.campo[fila][columna];
                let casillaHTML = document.createElement("button");
                casillaHTML.setAttribute("id", "casilla_" + fila + "_" + columna);
                casillaHTML.setAttribute("class","casillaCampo");
                casillaHTML.innerHTML="&nbsp;";
                divBuscaminas.appendChild(casillaHTML);
            }
            let salto=document.createElement("br");
            divBuscaminas.appendChild(salto);
        }
    }

    mostrarMinas(){

        for (let fila = 0; fila < this.miCampo._tamaño; fila++) {
            for (let columna = 0; columna < this.miCampo._tamaño; columna++) {
                let casilla = this.miCampo.campo[fila][columna];

                if(casilla.minada==true){
                    let casillaHTML=document.getElementById("casilla_" + fila + "_" + columna);
                    casillaHTML.innerHTML="*";
                    casillaHTML.innerHTML='💣';
                    
                }
            }
        }
    }

    mostrarBombasAlrededor(){

        for (let fila = 0; fila < this.miCampo._tamaño; fila++) {
            for (let columna = 0; columna < this.miCampo._tamaño; columna++) {
                let casilla = this.miCampo.campo[fila][columna];
                let casillaHTML=document.getElementById("casilla_" + fila + "_" + columna);
                casillaHTML.innerHTML=casilla._bombasAlrededor;
            }
        }
    }


}
