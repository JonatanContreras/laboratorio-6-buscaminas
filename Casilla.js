class Casilla {

    //Atributos

    /**
    * Estado de la casilla.
    * @type {"incognita"|"bandera"|"cubierta"|"descubierta"} 
    */
    _estado;
    /**
     * Cantidad de bombas que se encuentran alrededor de la casilla.
     * @type {number} 
     */
    _bombasAlrededor;

    /**
     * Propiedad para definir si existe una bomba en la casilla
     * @type {boolean}
     */
    _minada;

    constructor(){
        this._estado="cubierta";
        this._bombasAlrededor=0;
        this._minada=false;
    }
    

    set estado(valor="cubierta"){
        if(valor==="incognita" | valor==="bandera" | valor==="descubierta"){
            this._estado=valor;
        }
    }

    set bombasAlrededor(valor="0"){
        if(valor>=0 && valor<=8){
            this._bombasAlrededor=valor;
        }
    }

    get bombasAlrededor(){
        
        return this._bombasAlrededor;
    }

    set minada(valor=true){

        this._minada=valor;

    }

    get minada(){
        return this._minada;
    }
}

