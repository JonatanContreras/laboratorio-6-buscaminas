class Sembrador {
    /**
     * Campo que se debe sembrar
     * @typeof {Campo}
     */
    miCampo;

    /**
     * Número de minas que se den sembrar.
     * @typeof {number}
     */
    cantidadMinas;

    /**
     * Crea un sembrador.
     * @param {Campo} unCampo 
     * @param {number} cantidadMinas 
     */

    constructor(unCampo = "", cantidadMinas = 0) {

        //Verificar la validez de los argumentos

        if (isNaN(cantidadMinas)) {
            throw new Error("La cantidad debe ser numérica.");
        } else if (unCampo === "") {
            throw new Error("No se asignó un campo para sembrar.");
        } else if (!(unCampo instanceof Campo)) {
            throw new Error("La clase del campo no es la esperada.");
        }

        //Si no se ha disparado un error entonces asigna los valores a los atributos
        this.miCampo = unCampo;

        //Un número inválido de minas genera un valor predeterminado del 10% de casillas
        if (cantidadMinas < 1 || cantidadMinas > Math.pow(this.miCampo._tamaño, 2)) {
            this.cantidadMinas = Math.round(Math.pow(this.miCampo._tamaño, 2) * 0.1);
        } else {
            this.cantidadMinas = cantidadMinas;
        }
    }

    /**
     * Siembra cantidadMinas en miCampo. Modifica el valor del atributo minado de algunas casillas
     * @return {boolean} true
     */
    sembrarMinas() {

        while (this.cantidadMinas > 0) {
            let fila = Math.floor(Math.random() * this.miCampo._tamaño);
            let columna = Math.floor(Math.random() * this.miCampo._tamaño);

            if (this.miCampo.campo[fila][columna].minada === false) {
                this.miCampo.campo[fila][columna].minada = true;
                //Calcular las minas que existen alrededor de las casillas adyacentes
                this.calcularTotalMinas(fila, columna);
                this.cantidadMinas--;
            }
        }

        return true;
    }

    /**
     *  Aumenta en 1 el valor del atributo bombasAlrededor de cada una de las casillas subyacentes
     * a la casilla en la cual se plantó una bomba
     * 
     * @param {number} fila 
     * @param {number} columna 
     */

    calcularTotalMinas(fila, columna) {

        for (let x = -1; x < 2; x++) {
            for (let y = -1; y < 2; y++) {

                //Verificar no corresponda a la propia casilla y que la casilla esté dentro del campo
                if ( fila + x >= 0
                    && fila + x < this.miCampo._tamaño
                    && columna + y >= 0
                    && columna + y < this.miCampo._tamaño) {
                    let total = miCampo.campo[fila + x][columna + y].bombasAlrededor;
                    miCampo.campo[fila + x][columna + y].bombasAlrededor = total + 1;
                }
            }
        }
        return true;
    }
}

